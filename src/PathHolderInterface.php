<?php

namespace Drupal\path_holder;

use Drupal\Core\Entity\ContentEntityInterface;

interface PathHolderInterface extends ContentEntityInterface {

  /**
   * @return string
   */
  public function getPath();

}
