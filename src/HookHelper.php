<?php

namespace Drupal\path_holder;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Implement hooks for path_holder.module.
 *
 * This class is not necessary for implementing a minimal entity.
 */
class HookHelper {

  /**
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;


  public function __construct(LanguageManagerInterface $languageManager, ConfigFactoryInterface $configFactory) {
    $this->languageManager = $languageManager;
    $this->configFactory = $configFactory;
  }

  public function onContentPresave(PathHolderInterface $content) {
    if (!empty($content->_path_holder)) {
      return;
    }
    $name = 'path_holder.' . $content->uuid();
    foreach ($content->getTranslationLanguages() as $langcode => $language) {
      if ($language->isDefault()) {
        $config = $this->configFactory->getEditable($name);
      }
      elseif (method_exists($this->languageManager, 'getLanguageConfigOverride')) {
        $config = $this->languageManager->getLanguageConfigOverride($langcode, $name);
      }
      else {
        throw new \LogicException('Translated content without multilingual language manager. Giving up.');
      }
      foreach (static::getFields($content) as $fieldName => $fieldItemList) {
        $isBase = $fieldItemList->getFieldDefinition() instanceof BaseFieldDefinition;
        $key = $isBase ? $fieldName : "extra.$fieldName";
        if ($fieldItemList->isEmpty()) {
          $config->clear($key);
        }
        else {
          /** @var \Drupal\Core\Field\FieldItemInterface $fieldItem */
          $fieldItem = $fieldItemList->get(0);
          $value = $fieldItem->get($fieldItem->mainPropertyName())->getValue();
          if (!$isBase) {
            // Almost surely this is the layout.
            $value = serialize($fieldItemList->getValue());
          }
          $config->set($key, $value);
        }
      }
      $config->_path_holder = TRUE;
      $config->save();
      unset($config->_path_holder);
    }
  }

  public function onContentDelete(PathHolderInterface $content) {
    if (!empty($content->_path_holder)) {
      return;
    }
    $config = $this->configFactory->getEditable('path_holder.' . $content->uuid());
    $config->_path_holder = TRUE;
    $config->delete();
    unset($config->_path_holder);
  }

  /**
   * @param \Drupal\path_holder\PathHolderInterface $pathHolder
   *
   * @return \Drupal\Core\Field\FieldItemListInterface[]
   *   Configured fields and the label.
   */
  public static function getFields(PathHolderInterface $pathHolder) {
    $fields = $pathHolder->getFields();
    $keys = $pathHolder->getEntityType()->getKeys();
    unset($keys['label']);
    return array_diff_key($fields, array_flip($keys));
  }

}
