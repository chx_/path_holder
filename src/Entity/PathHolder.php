<?php

namespace Drupal\path_holder\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\path_holder\PathHolderInterface;

/**
 * Defines the path holder entity class.
 *
 * @ContentEntityType(
 *   id = "path_holder",
 *   label = @Translation("Path holder"),
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\sd8\AllowViewAccessHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   admin_permission = "administer path holders",
 *   base_table = "path_holder",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "title",
 *     "langcode" = "langcode"
 *   },
 *   field_ui_base_route = "view.admin_path_holder.page_1",
 *   links = {
 *     "canonical" = "/path_holder/{path_holder}",
 *     "add-form" = "/path_holder/add",
 *     "delete-form" = "/path_holder/{path_holder}/delete",
 *     "edit-form" = "/path_holder/{path_holder}/edit",
 *   }
 * )
 */
class PathHolder extends ContentEntityBase implements PathHolderInterface {

  /**
   * @return string
   */
  public function getPath() {
    return $this->get('path')->alias;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);
    $fields['path'] = BaseFieldDefinition::create('path')
        ->setLabel(t('URL alias'))
        ->setTranslatable(TRUE)
        ->setDisplayOptions('form', [
          'type' => 'path',
          'weight' => 30,
        ])
        ->setComputed(TRUE);
    return $fields;
  }

}
