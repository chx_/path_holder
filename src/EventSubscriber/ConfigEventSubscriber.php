<?php

namespace Drupal\path_holder\EventSubscriber;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\StorableConfigBase;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\language\Config\LanguageConfigOverrideCrudEvent;
use Drupal\language\Config\LanguageConfigOverrideEvents;
use Drupal\path_holder\Entity\PathHolder;
use Drupal\path_holder\HookHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Copy / delete path holder config to content.
 *
 * This class is not necessary for implementing a minimal entity.
 */
class ConfigEventSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * ConfigEventSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   */
  public function __construct(EntityRepositoryInterface $entityRepository) {
    $this->entityRepository = $entityRepository;
  }

  /**
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   */
  public function onConfigSave(ConfigCrudEvent $event) {
    $this->saveContent($event->getConfig());
  }

  /**
   * @param \Drupal\language\Config\LanguageConfigOverrideCrudEvent $event
   */
  public function onTranslatedConfigSave(LanguageConfigOverrideCrudEvent $event) {
    $languageConfigOverride = $event->getLanguageConfigOverride();
    $this->saveContent($languageConfigOverride, $languageConfigOverride->getLangcode());
  }

  /**
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   */
  public function onConfigDelete(ConfigCrudEvent $event) {
    $this->deleteContent($event->getConfig());
  }

  /**
   * @param \Drupal\language\Config\LanguageConfigOverrideCrudEvent $event
   */
  public function onTranslatedConfigDelete(LanguageConfigOverrideCrudEvent $event) {
    $languageConfigOverride = $event->getLanguageConfigOverride();
    $this->deleteContent($languageConfigOverride, $languageConfigOverride->getLangcode());
  }

  /**
   * @param \Drupal\Core\Config\StorableConfigBase $config
   * @param string|null $langcode
   */
  protected function saveContent(StorableConfigBase $config, $langcode = NULL) {
    $uuid = $this->getUuid($config);
    if ($uuid === FALSE) {
      return;
    }
    /** @var \Drupal\Core\Entity\ContentEntityInterface $content */
    if (!$content = $this->loadContentEntity($uuid)) {
      $content = PathHolder::create(['uuid' => $uuid]);
    }
    if (isset($langcode)) {
      $content = $content->getTranslation($langcode);
    }
    foreach (HookHelper::getFields($content) as $fieldName => $fieldItemList) {
      if ($fieldItemList->getFieldDefinition() instanceof BaseFieldDefinition) {
        $fieldItemList->setValue($config->get($fieldName));
      }
      else {
        $fieldItemList->setValue(unserialize($config->get("extra.$fieldName")));
      }
    }
    $content->_path_holder = TRUE;
    $content->save();
    unset($content->_path_holder);
  }

  /**
   * @param \Drupal\Core\Config\StorableConfigBase $config
   * @param string|null $langcode
   */
  protected function deleteContent(StorableConfigBase $config, $langcode = NULL) {
    $uuid = $this->getUuid($config);
    if ($uuid === FALSE) {
      return;
    }
    /** @var \Drupal\Core\Entity\ContentEntityInterface $content */
    if ($content = $this->loadContentEntity($uuid)) {
      $content->_path_holder = TRUE;
      if (isset($langcode)) {
        $content->removeTranslation($langcode);
        $content->save();
      }
      else {
        $content->delete();
      }
      unset($content->_path_holder);
    }
  }

  /**
   * @param \Drupal\Core\Config\StorableConfigBase $config
   *
   *
   * @return string|FALSE
   *   Either the UUID or FALSE if this is not a path holder config object.
   *   Also FALSE if the config object is being saved/deleted by HookHelper.
   */
  protected function getUuid(StorableConfigBase $config) {
    $uuid = preg_replace('/^path_holder./', '', $config->getName(), 1, $count);
    return $count && empty($config->_path_holder) ? $uuid : FALSE;
  }

  /**
   * @param string $uuid
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   */
  protected function loadContentEntity($uuid) {
    return $this->entityRepository->loadEntityByUuid('path_holder', $uuid);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::SAVE] = ['onConfigSave'];
    $events[ConfigEvents::DELETE] = ['onConfigDelete'];
    if (class_exists('Drupal\language\Config\LanguageConfigOverrideEvents')) {
      $events[LanguageConfigOverrideEvents::SAVE_OVERRIDE] = ['onTranslatedConfigSave'];
      $events[LanguageConfigOverrideEvents::DELETE_OVERRIDE] = ['onTranslatedConfigDelete'];
    }
    return $events;
  }

}
